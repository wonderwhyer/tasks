import {ITask} from "../middle/TaskTypes";
export enum DB_STATE {
    OFFLINE,
    ERROR,
    COMPLETE,
    IN_SYNC,
    SYNCING,
    DENIED,
    SIGN_IN_NEEDED
}

var STATE_LABELS:{[key:number]:string} = {};
STATE_LABELS[DB_STATE.COMPLETE] = 'Offline';
STATE_LABELS[DB_STATE.OFFLINE] = 'Offline';
STATE_LABELS[DB_STATE.ERROR] = 'Error';
STATE_LABELS[DB_STATE.IN_SYNC] = 'In sync';
STATE_LABELS[DB_STATE.SYNCING] = 'Syncing';
STATE_LABELS[DB_STATE.DENIED] = 'Denied';
STATE_LABELS[DB_STATE.SIGN_IN_NEEDED] = 'SIGN In!';
export function getStateLabel(state:DB_STATE):string {
    console.log(state);
    return STATE_LABELS[state];
}

export interface IDB {
    
    state:DB_STATE;
    offline:string;
    order:{
        _id:string,
        ids:string[]
    };
    groupNames:{[key:number]:string},
    tasks:{[key:string]:ITask};
    
    newTask:(task:ITask, order:number) => PromiseLike<void>;
    updateTask:(tak:ITask) => PromiseLike<void>;
    onChange:()=>void;
    clear: () => void;
    reSync: () => void;
}


