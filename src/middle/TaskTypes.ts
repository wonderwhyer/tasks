export enum TASK_STATE {
    INCOMPLETE,
    COMPLETE
}

export interface ITask {
    state:TASK_STATE;
    group:number;
    text:string;
    _id:string;
    _rev?:string;
}

