import PouchDB = require('pouchdb');
import {ITask, TASK_STATE} from "../middle/TaskTypes";
import {DB_STATE, IDB} from "../middle/DBModule";


var db = (function () {
    var db = new PouchDB('tasks');
    db.info().then(function (info) {
        console.log(info);
    });
    connectRemote(db);
    return db;
})();
initData();


function changeState(state:DB_STATE) {
    edb.state = state;
    change();
}
var sync;
function connectRemote(db) {
    if (sync) {
        sync.cancel();
        sync = null;
    }
    var remoteDB = new PouchDB(window.location.origin + '/db/tasks');
    sync = db.sync(remoteDB, {
        live: true,
        retry: true,
        back_off_function: function (lastTimeout) {
            console.log('backoff', arguments);
            changeState(DB_STATE.OFFLINE);
            return lastTimeout == 0 ? 100 : Math.min(lastTimeout * 2, 1000);
        }
    }).on('change', function (change) {
        console.log('change', change);
        console.log(sync, sync.pull.state, sync.push.state);
    }).on('error', function (err) {
        console.log('error', err);
        changeState(DB_STATE.ERROR);
        console.log(sync, sync.pull.state, sync.push.state);
    }).on('complete', function (err) {
        console.log('complete', err);
        console.log(sync, sync.pull.state, sync.push.state);
        var errors = err.push.errors.concat(err.pull.errors);
        for (var i = 0; i < errors.length; i++) {
            var error = errors[i];
            if (error.status == 401) {
                changeState(DB_STATE.SIGN_IN_NEEDED);
                return;
            }
        }
        changeState(DB_STATE.COMPLETE);
    }).on('paused', function (err) {
        console.log('paused', arguments);
        changeState(DB_STATE.IN_SYNC);
        console.log(sync, sync.pull.state, sync.push.state);
    }).on('active', function (err) {
        console.log('active', err);
        changeState(DB_STATE.SYNCING);
        console.log(sync, sync.pull.state, sync.push.state);
    }).on('denied', function (err) {
        console.log('denied', err);
        changeState(DB_STATE.DENIED);
        console.log(sync, sync.pull.state, sync.push.state);
    });
    sync.catch(function () {
        console.log('catch', arguments);
    });
    console.log(sync);
}

var taskDoc;
function initData() {
    db.allDocs({
        include_docs: true
    }).then(function (allDocs) {
        applyRows(allDocs.rows.map((row) => row.doc));
        listenToChanges();
        change();
    }).catch(function (err) {
        console.log('error getting tasks', err);
    });
}

function applyRows(rows) {
    rows.forEach((doc) => {
        if (doc._id == 'order') {
            edb.order = doc;
        } else if (doc._id == 'groupNames') {
            edb.groupNames = doc;
        } else if (doc._deleted) {
            delete edb.tasks[doc._id];
        } else {
            edb.tasks[doc._id] = doc;
        }
    });
}

var started = false;
function listenToChanges() {
    started = true;
    db.changes({
        since: 'now',
        live: true,
        include_docs: true
    })['on']('change', function (changes) {
        applyRows([changes.doc]);
        change();
    }).on('complete', function (info) {
        console.log(info);
    }).on('error', function (err) {
        console.log(err);
    });
}

function change() {
    if (started && edb.onChange) {
        edb.onChange();
    }
}

function removeFromOrder(id) {
    var index = edb.order.ids.indexOf(id);
    if (index > -1) {
        edb.order.ids.splice(index, 1);
    }
}

function removeTask(task:ITask) {
    db.remove(task, {
        rev: task._rev
    }).then((response) => {
        console.log('remove', response);
    }).catch((error) => {
        console.log('remove error', error);
    });
    removeFromOrder(task._id);
}

var edb:IDB = {
    state: DB_STATE.SYNCING,
    offline: 'i',
    tasks: {},
    order: {
        _id: 'order',
        ids: []
    },
    groupNames: {
        _id: 'groupNames',
        0: 'ungrouped'
    },
    newTask: (task, index) => {
        edb.order.ids.splice(index, 0, task._id);
        return db.bulkDocs([
            task,
            edb.order
        ]).then((response) => {
            console.log(response);
        }).catch((error) => {
            console.log(error);
        })
    },
    updateTask: (task:ITask) => {
        return db.put(task).then((response) => {
            console.log(response);
        }).catch((error) => {
            console.log(error);
        });
    },
    clear: () => {
        Object.keys(edb.tasks)
            .map((key) => edb.tasks[key])
            .filter((task:ITask) => task.state == TASK_STATE.COMPLETE)
            .forEach(removeTask);
        db.put(edb.order).then((response) => {
            console.log(response);
        }).catch((error) => {
            console.log(error);
        })
    },
    reSync: () => {
        connectRemote(db);
    },
    onChange: null
};
/*
 function getConflicts() {
 return db.get('tasks', {
 revs: true,
 revs_info: true,
 conflicts: true
 }).then((doc1) => {
 if (!doc1._conflicts || doc1._conflicts.length == 0) {
 return Promise.resolve(doc1);
 } else if (doc1._conflicts.length > 1) {
 alert(doc1);
 } else if (doc1._conflicts.length == 1) {
 return db.get('tasks', {
 rev: doc1._conflicts[0]
 }).then((doc2) => {
 console.log('conflict', doc1, doc2);
 return db.remove(doc2);
 });
 }
 //console.log(doc);
 });
 }
 */

export = edb;