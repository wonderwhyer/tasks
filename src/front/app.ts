import snabbdom = require('snabbdom');
import sclass = require('snabbdom/modules/class'); // makes it easy to toggle classes
import props =    require('snabbdom/modules/props'); // for setting properties on DOM elements
import style =    require('snabbdom/modules/style'); // handles styling on elements with support for animations
import eventlisteners =    require('snabbdom/modules/eventlisteners'); // attaches event listeners
var patch = snabbdom.init([sclass, props, style, eventlisteners]);
import h = require('snabbdom/h');
import edb = require('./db');
import {IDB, getStateLabel, DB_STATE} from '../middle/DBModule';
import {ITask, TASK_STATE} from '../middle/TaskTypes';
import {create} from "domain";

edb.onChange = () => {
    app.update(edb);
    if (Object.keys(edb.tasks).length == 0) {
        newTaskAfter(null);
    }
}

initServiceWorker();
function initServiceWorker() {
    if ('serviceWorker' in navigator) {
        navigator['serviceWorker'].register('/serviceWorker.js').then(function (registration) {
            // Registration was successful
            console.log('ServiceWorker registration successful with scope: ', registration.scope);
            edb.offline = 'r';
        }).catch(function (err) {
            // registration failed :(
            edb.offline = 'f';
            console.log('ServiceWorker registration failed: ', err);
        });
    } else {
        edb.offline = 'u';
    }
}

function newTaskAfter(task:ITask,text:string='') {
    var newTask = {
        _id: Math.random().toString(),
        text: text,
        state: TASK_STATE.INCOMPLETE
    };
    app.focus = newTask._id;
    var index = task ? edb.order.ids.indexOf(task._id) + 1 : 0;
    edb.newTask(newTask, index);
}

function getOrderedTasks(state:IDB):ITask[] {
    Object.keys(state.tasks).forEach((key:string) => {
        if (state.order.ids.indexOf(key) < 0) {
            state.order.ids.push(key);
        }
    });
    return state.order.ids.filter((id:string) => !!state.tasks[id]).map((id) => state.tasks[id]);
}

function createCookie(name:string, value:string, days?) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = ""
    }

    document.cookie = name + "=" + value + expires + "; path=/";
    edb.reSync();
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

class App {
    private vnode:VNode;
    focus:string;

    constructor(state:IDB, private container:Element) {
        this.vnode = this.render(state);
        patch(container, this.vnode);
    }

    private taskList(tasks:ITask[], focus_id?:string) {
        console.log(tasks.length);
        return h('div.tasks.collection', tasks.map((task:ITask) => {
            return Task(task, task._id == focus_id)
        }));
    }

    private stateList(state:DB_STATE) {
        var res = [];
        for (var i = 0; i < 7; i++) {
            res.push(h('div#state_' + i + '.btn', {
                key: 'state_' + i,
                class: {
                    red: i != DB_STATE.IN_SYNC && i != DB_STATE.SYNCING,
                    green: i == DB_STATE.IN_SYNC,
                    blue: i == DB_STATE.SYNCING,
                },
                style: state == i ? {
                    width: '11em',
                    opacity: '1',
                    position: 'absolute',
                    top: '-1em',
                    transition: 'none',
                    delayed: {
                        opacity: '1',
                        top: '0em',
                        transition: 'all 1s',
                        'z-index': 1
                    }
                } :
                {
                    width: '11em',
                    opacity: '0',
                    position: 'absolute',
                    top: '1em',
                    transition: 'all 1s',
                    'z-index': 0
                }
            }, getStateLabel(i)))
        }
        return h('div', {
            style: {
                position: 'relative',
                height: '36px',
                width: '11em',
                'vertical-align': 'middle',
                display: 'inline-block'
            }
        }, res);
    }

    private delete() {
        return h('div.btn-floating.green.waves-effect.waves-light.clear', {
            on: {
                click: () => edb.clear()
            }
        }, [h('i.material-icons.dp48.small', 'delete')]);
    }

    private add() {
        return h('div.btn-floating.green.waves-effect.waves-light.clear', {
            on: {
                click: () => newTaskAfter(null)
            }
        }, [h('i.material-icons.dp48.small', 'add')])
    }

    private offlineStatus() {
        if (edb.offline == 'i') {
            return h("div.preloader-wrapper.small.active", [
                h("div.spinner-layer.spinner-green-only", [
                    h("div.circle-clipper.left", [
                        h("div.circle")
                    ]),
                    h("div.gap-patch", [
                        h("div.circle")
                    ]),
                    h("div.circle-clipper.right", [
                        h("div.circle")
                    ])
                ])
            ]);
        }

        if (edb.offline == 'r') {
            return h('div.btn-floating.green.clear', [h('i.material-icons.dp48.small', 'thumb_up')])
        }

        if (edb.offline == 'f' || edb.offline == 'u') {
            return h('div.btn-floating.red.clear', [h('i.material-icons.dp48.small', 'thumb_down')])
        }
    }

    private auth() {
        var key = readCookie('auth');
        return h('div.col.s2.right', [
                h('input#password', {
                    props: {
                        value: key,
                        type: 'password',
                        placeholder: "Password"
                    },
                    on: {
                        change: (event:Event) => {
                            var value = (<HTMLInputElement>event.currentTarget).value;
                            createCookie('auth', value,1000);
                        }
                    }
                })
            ]
        );
    }

    render(state:IDB, focus_id?:string) {
        return h('div.app', [
            h('nav.header.blue', [
                    this.stateList(state.state),
                    this.delete(),
                    this.add(),
                    this.offlineStatus(),
                    this.auth()
                ]
            ),
            this.taskList(getOrderedTasks(state), focus_id)
        ]);
    }

    update(state:IDB) {
        var newVNode = this.render(state, this.focus);
        if (state.tasks[this.focus]) {
            this.focus = undefined;
        }
        this.vnode = patch(this.vnode, newVNode);
    }
}

function Task(state:ITask, focus:boolean) {
    function textChange(event:Event) {
        state.text = (<HTMLInputElement>event.currentTarget).value;
        edb.updateTask(state);
        //app.update(edb);
    }

    function paste(e:ClipboardEvent) {
        var pastedText;
        if (window['clipboardData'] && window['clipboardData'].getData) { // IE
            pastedText = window['clipboardData'].getData('Text');
        } else if (e.clipboardData && e.clipboardData.getData) {
            pastedText = e.clipboardData.getData('text/plain');
        }
        //alert(pastedText); // Process and handle text...

        if(pastedText.indexOf('\n')>-1) {
            var parts = pastedText.split('\n');
            pastedText = parts[0];
            for (var i = 1; i < parts.length; i++) {
                var part = parts[i];
                newTaskAfter(state,part);
            }
        }
        state.text = pastedText;
        edb.updateTask(state);
        e.preventDefault();
    }

    function statusChange(event) {
        state.state = (<HTMLInputElement>event.currentTarget).checked ? TASK_STATE.COMPLETE : TASK_STATE.INCOMPLETE;
        edb.updateTask(state);
        //app.update(edb);
    }

    function keyDown(event:KeyboardEvent) {
        console.log(state);
        if (event.keyCode == 13) {
            var target:HTMLInputElement = <HTMLInputElement>event.target;
            var start = target.selectionStart;
            var end = target.selectionEnd;
            var leftText;
            var rightText;
            if(start==end) {
                leftText = target.value.substring(0,start);
                rightText = target.value.substring(start);
            } else {
                leftText = target.value.substring(0,start)+target.value.substring(end);
                rightText = target.value.substring(start,end);
            }

            state.text = leftText;
            edb.updateTask(state);
            newTaskAfter(state,rightText);
        }
    }

    return h('div.task.row.collection-item',
        {
            key: state._id,
            props: {
                id: state._id
            },
            class: {
                completed: state.state == TASK_STATE.COMPLETE,
                focus: focus
            },
            style: {
                'background-color': 'white',
                opacity: '0',
                height: '0',
                'margin-top': '-77px',
                transition: 'all 0.3s ease-in-out',
                delayed: {height: '77px', opacity: '1', 'margin-top': '0px'}
            }
        },
        [
            h('div.text.input-field.col.s12', [
                    h('input.text', {
                        hook: {
                            insert: function (vnode:VNode) {
                                if (focus) {
                                    (<HTMLElement>vnode.elm).focus();
                                    (<HTMLElement>vnode.elm)['selectionStart'] = 0;
                                    (<HTMLElement>vnode.elm)['selectionEnd'] = 0;
                                }
                            }
                        },
                        props: {
                            value: state.text,
                            type: 'text'
                        },
                        on: {
                            change: textChange,
                            keydown: keyDown,
                            paste: paste
                        }
                    })
                ]
            ),
            h('div.input-field', {
                    style: {
                        position: 'absolute',
                        top: '0px',
                        right: '0px',
                        padding: '0 10px'
                    }
                }, [
                    h('input',
                        {
                            props: {
                                id: 'check' + state._id,
                                type: 'checkbox',
                                checked: state.state == TASK_STATE.COMPLETE
                            },
                            on: {
                                change: statusChange
                            }
                        }
                    ),
                    h('label', {
                            hook: {
                                insert: function (vnode:VNode) {
                                    vnode.elm.setAttribute('for', 'check' + state._id)
                                }
                            }
                        },
                        ''
                    )
                ]
            ),
        ]
    )

}

var content = document.getElementsByClassName('content')[0];
var app = new App(edb, content);