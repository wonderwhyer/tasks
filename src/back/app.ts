/// <reference path="../../typings/main.d.ts" />
import http = require('http');

import express = require('express');
import path = require('path');
import favicon = require('serve-favicon');
import cookieParser = require('cookie-parser');
import bodyParser = require('body-parser');
import PouchDB = require('pouchdb');
import PouchExpress = require('express-pouchdb');

process.on('uncaughtException', function (err) {
    console.log(err);
});

import routes = require('./routes/index');
import {pass} from "./Pass";
//import {ITask, TASK_STATE} from "../middle/TaskTypes";

var app = express();

var pouchExpress = PouchExpress(PouchDB);
app.use(cookieParser());
app.use('/db', (req, res, next) => {
    console.log('auth', req.cookies);
    if (req.cookies.auth == pass) {
        next();
    } else {
        res.status(401).send('password needed');
    }
}, pouchExpress);

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

function conflictsOnly(row) {
    return row.doc._conflicts.length > 0
}

function rowsToDocs(row) {
    return row.doc;
}

var db = new PouchDB('tasks', (error, res) => {
    res.changes({
        since: 'now',
        live: true,
        //include_docs: true,
        conflicts: true
    }, function (err, change) {
        console.log('callback:', err, change);
    }).on('change', function (changes) {
        console.log('change:', changes);
        console.log(changes.doc.tasks[0]);
    }).on('complete', function (info) {
        console.log('complete:', info);
    }).on('error', function (err) {
        console.log('error:', err);
    });

    res.allDocs({
        conflicts: true,
        include_docs: true
    }).then((response) => {

        var conflictDocs = response.rows.filter(conflictsOnly).map(rowsToDocs);

        conflictDocs.forEach((doc) => {
            if (doc._id == 'order') {

            } else if (doc._id != 'tasks') {//TODO remove
                db.get(doc._id, {
                    conflicts: true
                }).then(console.log);
            }
        });

        console.log(conflictDocs);
    }).catch((err) => {
        console.log(err);
    });
});

/*function mergeTask(target:ITask, source:ITask) {
 if(target.state!=source.state) {
 target.state = TASK_STATE.INCOMPLETE;
 }
 }*/

/*
 function mergeTasks(revisions:ITask[][]) {
 var newTasks = [];
 var tasksMap = {};
 revisions.forEach((tasks:ITask[]) => {
 tasks.forEach((task:ITask) => {
 if (!tasksMap.hasOwnProperty(task._id)) {
 tasksMap[task._id] = task;
 newTasks.push(task);
 } else {
 mergeTask(tasksMap[task._id], task);
 }
 });
 });

 }

 function mergeTask(target:ITask, source:ITask) {
 if(target.state!=source.state) {
 target.state = TASK_STATE.INCOMPLETE;
 }
 }
 */
db.info().then(function (info) {
    console.log('database started at /db/tasks', info);
});

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));



var staticPath = path.resolve('./wwwroot', 'public');
console.log(staticPath);
app.use(express.static(staticPath));

app.use('/', routes);

app.use((req, res, next) => {
    var err = new Error('Not Found');
    err['status'] = 404;
    next(err);
});

if (app.get('env') === 'development') {

    app.use((err:any, req, res, next) => {
        res.status(err['status'] || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use((err:any, req, res, next) => {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

var port = process.env.PORT || '3003';
var server = http.createServer(app);
server.listen(port);

server.on('error', onError);
server.on('listening', onListening);
function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    console.log('Listening on ' + bind);
}