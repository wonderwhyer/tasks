/// <reference path='../../../typings/main.d.ts' />
import express = require('express');

var router = express.Router();

router.get('/', function (req, res, next) {
    res.render('index', {
        title: 'Tasker'
    });
});

export = router;