var webpack = require('webpack');
module.exports = {
    entry: {
        app: './src/front/app.ts'
    },
    output: {
        path: __dirname + "/wwwroot/public/js",
        filename: '[name].js'
    },
    devtool: 'source-map',
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
    },
    module: {
        loaders: [
            {test: /\.ts$/, loader: 'ts-loader'}
        ]
    }
}